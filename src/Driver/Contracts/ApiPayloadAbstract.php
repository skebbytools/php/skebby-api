<?php
/*
 * Copyright (c) 2024. Amedeo Lanza - ConsuLanza Informatica.
 * This software is licensed under GNU AGPLv3 - https://www.gnu.org/licenses/agpl-3.0.en.html.
 * For commercial use you can obtain a limited commercial license from the author.
 *
 *
 */

declare(strict_types=1);

namespace Inforisorse\SkebbyApi\Driver\Contracts;

abstract class ApiPayloadAbstract implements ApiPayloadInterface
{
    private array $payload = [];

    public function getPayload(): array
    {
        $this->preparePayload();
        return $this->payload;
    }

    protected function addStringParamIfNotEmpty(string $paramName): void
    {
        if ($this->$paramName !== '') {
            $this->payload[$paramName] = $this->$paramName;
        }
    }

    protected function addPayloadParam(string $paramName, mixed $value): void
    {
        $this->payload[$paramName] = $value;
    }

    abstract protected function preparePayload(): void;

}
