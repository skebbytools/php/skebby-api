<?php
/*
 * Copyright (c) 2024. Amedeo Lanza - ConsuLanza Informatica.
 * This software is licensed under GNU AGPLv3 - https://www.gnu.org/licenses/agpl-3.0.en.html.
 * For commercial use you can obtain a limited commercial license from the author.
 *
 */
declare(strict_types=1);

namespace Inforisorse\SkebbyApi\Driver;

use Inforisorse\SkebbyApi\Driver\Contracts\ApiPayloadAbstract;
use Inforisorse\SkebbyApi\Enums\Encoding;
use Inforisorse\SkebbyApi\Enums\MessageType;
use Inforisorse\SkebbyApi\Exceptions\ValidationException;
use Inforisorse\SkebbyApi\Helper\Validator;

class Message extends ApiPayloadAbstract
{

    private const DEFAULT_MAX_FRAGMENTS = 7;

    /**
     * @param string $text
     * @param array $recipients
     * @param string $sender
     * @param MessageType $messsageType
     * @param string $scheduled_delivery_time
     * @param string $scheduled_delivery_timezone
     * @param string $order_id
     * @param bool $returnCredits
     * @param bool $returnRemaining
     * @param bool $allowInvalidRecipients
     * @param Encoding $encoding
     * @param int|null $id_landing
     * @param string $campaign_name
     * @param int $max_fragments
     * @param bool $truncate
     * @param int|null $validity_period_min
     * @param string $richsms_url
     */
    public function __construct(
        // The body of the message. *Message max length could be 160 chars when using low-quality SMSs.
        // New line char needs to be codified as “\n”.
        protected string      $text,

        // A list of recipents phone numbers. The recipients’ numbers must be in the international format
        // (+393471234567 or 00393471234567)
        protected array       $recipients,

        // The Sender name. If the message type allows a custom TPOA and the field is left empty, the user’s
        // preferred TPOA is used. Must be empty if the message type does not allow a custom TPOA
        protected string      $sender = '',

        // The type of SMS.
        protected MessageType $messsageType = MessageType::Classic,

        // The messages will be sent at the given scheduled time
        // String [ddMMyy, yyyyMMdd, ddMMyyHHmm, yyyyMMddHHmmss, yyyy-MM-dd HH:mm, yyyy-MM-dd HH:mm.0]
        protected string      $scheduled_delivery_time = '',

        // Optional timezone applied to scheduled_delivery_time date
        protected string      $scheduled_delivery_timezone = '',

        // Specifies a custom order ID
        // String (max 32 chars, accepts only any letters, numbers, underscore, dot and dash)
        protected string      $order_id = '',

        //Returns the number of used sms credits. Note that it can be greater than sent SMS number: i.e. when
        // message is more than 160 chars long, each SMS will consume more than one credit according to its length
        protected bool        $returnCredits = false,

        // Returns the number of remaining SMS credits for the quality used in the sent SMS and for the default
        //user nation
        protected bool        $returnRemaining = false,

        // Sending to an invalid recipient does not block the operation
        protected bool        $allowInvalidRecipients = false,

        // The SMS encoding. Use UCS2 for non standard character sets
        // String (“gsm” or “ucs2”)
        protected Encoding    $encoding = Encoding::Gsm,

        // The id of the published page. Also add the %PAGESLINK____________% placeholder in the message body
        protected ?int        $id_landing = null,

        // The campaign name (max 64 characters)
        protected string      $campaign_name = '',

        // The number of maximum fragments allowed per sms
        protected int         $max_fragments = self::DEFAULT_MAX_FRAGMENTS,

        // True, truncates any message exceeding max_fragments, False doesn’t send any overly long sms
        protected bool        $truncate = true,

        // Period after which the SMS message will be deleted from the SMS center. Please note the set expiry
        // time may not be honoured by some mobile networks.
        protected ?int        $validity_period_min = null,

        // The url where the rich url redirects. Also add the %RICHURL________% placeholder in the message body
        protected string      $richsms_url = ''
    )
    {
        $this->withRecipients($recipients);
    }

    /**
     * @throws ValidationException
     */
    public function withRecipients(array $recipients): Message
    {
        $this->recipients = [];
        foreach ($recipients as $recipient) {
            $this->addRecipient($recipient);
        }
        return $this;
    }

    /**
     * @throws ValidationException
     */
    public function addRecipient(string $recipient): Message
    {
        $this->recipients[] = Validator::phoneNumber($recipient, 'recipient');
        return $this;
    }

    /**
     * @throws ValidationException
     */
    public function withText(string $text): Message
    {
        $this->text = Validator::messageText($text, $this->encoding);
        return $this;
    }

    /**
     * @throws ValidationException
     */
    public function withSender(string $sender): Message
    {
        $this->sender = Validator::sender($sender, $this->messsageType);
        return $this;
    }

    public function withMesssageType(MessageType $messageType): Message
    {
        $this->messsageType = $messageType;
        return $this;
    }

    /**
     * @throws ValidationException
     */
    public function withScheduledDeliveryTime(string $scheduled_delivery_time): Message
    {
        $this->scheduled_delivery_time = Validator::packedDateTime($scheduled_delivery_time, 'scheduled_delivery_time');
        return $this;
    }

    public function withScheduledDeliveryTimezone(string $scheduled_delivery_timezone): Message
    {
        $this->scheduled_delivery_timezone = Validator::ianaTimeZone($scheduled_delivery_timezone, 'anaTimeZone');
        return $this;
    }

    /**
     * @throws ValidationException
     */
    public function withOrderId(string $order_id): Message
    {
        $this->order_id = Validator::orderId($order_id, 'order_id');
        return $this;
    }

    public function withReturnCredits(bool $returnCredits = true): Message
    {
        $this->returnCredits = $returnCredits;
        return $this;
    }

    public function withReturnRemaining(bool $returnRemaining = true): Message
    {
        $this->returnRemaining = $returnRemaining;
        return $this;
    }

    public function withAllowInvalidRecipients(bool $allowInvalidRecipients): Message
    {
        $this->allowInvalidRecipients = $allowInvalidRecipients;
        return $this;
    }

    public function withEncoding(Encoding $encoding): Message
    {
        $this->encoding = $encoding;
        return $this;
    }

    public function withIdLanding(?int $id_landing): Message
    {
        $this->id_landing = $id_landing;
        return $this;
    }

    /**
     * @throws ValidationException
     */
    public function withCampaignName(string $campaign_name): Message
    {
        $this->campaign_name = Validator::campaignName($campaign_name, 'campaign_name');
        return $this;
    }

    public function withMaxFragments(int $max_fragments): Message
    {
        $this->max_fragments = $max_fragments;
        return $this;
    }

    public function withTruncate(bool $truncate): Message
    {
        $this->truncate = $truncate;
        return $this;
    }

    public function withValidityPeriodMin(?int $validity_period_min): Message
    {
        $this->validity_period_min = $validity_period_min;
        return $this;
    }

    public function withRichsmsUrl(string $richsms_url): Message
    {
        $this->richsms_url = $richsms_url;
        return $this;
    }

    protected function preparePayload(): void
    {
        $this->addPayloadParam('message_type', $this->messsageType->value);
        $this->addPayloadParam('message', $this->text);
        $this->addPayloadParam('recipient', $this->recipients);

        if ($this->messsageType->canForceSender() && $this->sender !== '') {
            $this->addPayloadParam('sender', $this->sender);
        }

        $this->addStringParamIfNotEmpty('scheduled_delivery_time');
        $this->addStringParamIfNotEmpty('scheduled_delivery_timezone');
        $this->addStringParamIfNotEmpty('order_id');

        if ($this->returnCredits) {
            $this->addPayloadParam('returnCredits', $this->returnCredits);
        }

        if ($this->returnRemaining) {
            $this->addPayloadParam('returnRemaining', $this->returnRemaining);
        }

        if ($this->allowInvalidRecipients) {
            $this->addPayloadParam('allowInvalidRecipients', $this->allowInvalidRecipients);
        }

        if ($this->encoding !== Encoding::Gsm) {
            $this->addPayloadParam('encoding', $this->encoding->value);
        }

        if ($this->id_landing !== null) {
            $this->addPayloadParam('id_landing', $this->id_landing);
        }

        $this->addStringParamIfNotEmpty('campaign_name');

        if ($this->max_fragments !== self::DEFAULT_MAX_FRAGMENTS) {
            $this->addPayloadParam('max_fragments', $this->max_fragments);
        }

        if (!$this->truncate) {
            $this->addPayloadParam('truncate', $this->truncate);
        }

        if ($this->validity_period_min !== null) {
            $this->addPayloadParam('validity_period_min', $this->validity_period_min);
        }

        $this->addStringParamIfNotEmpty('richsms_url');
    }
}
