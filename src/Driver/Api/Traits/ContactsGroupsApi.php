<?php
/*
 * Copyright (c) 2024. Amedeo Lanza - ConsuLanza Informatica.
 * This software is licensed under GNU AGPLv3 - https://www.gnu.org/licenses/agpl-3.0.en.html.
 * For commercial use you can obtain a limited commercial license from the author.
 *
 *
 */

namespace Inforisorse\SkebbyApi\Driver\Api\Traits;

use stdClass;

trait ContactsGroupsApi
{
    public function createContactsGroup(string $name, string $description)
    {
        $query = [
            'name' => $name,
            'description' => $description
        ];
        return $this->simplePostQuery('group', $query);
    }

    public function listContactsGroups(int $pageNumber = 0, int $pageSize = 0): stdClass
    {
        return $this->simpleGetQuery('groups', []);
    }
}
