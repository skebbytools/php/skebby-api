<?php
/*
 * Copyright (c) 2024. Amedeo Lanza - ConsuLanza Informatica.
 * This software is licensed under GNU AGPLv3 - https://www.gnu.org/licenses/agpl-3.0.en.html.
 * For commercial use you can obtain a limited commercial license from the author.
 *
 *
 */

namespace Inforisorse\SkebbyApi\Driver\Api\Traits;

use stdClass;

trait UserApi
{
    /**
     * API used to retrieve the dashboard URL of the authenticated user
     * @return stdClass
     */
    public function getDashboard(): ?string
    {
        return $this->simpleGetQuery('dashboard', []);
    }

    /**
     * Retrieve the SMS Credits and email service status of the user.
     *
     * @param bool $getMoney
     * @param bool $typeAliases
     * @return stdClass
     */
    public function getSmsCreditsAndEmailServiceStatus(bool $getMoney = false, bool $typeAliases = true): stdClass
    {
        $query = [
            'getMoney' => $getMoney,
            'typeAliases' => $typeAliases
        ];
        return $this->simpleGetQuery('status', $query);
    }

    /**
     * Checks whether the user session is still active and valid (without renewal).
     * Note: with Token authentication always returns false
     *
     * @return bool
     */
    public function checkSession(): bool
    {
        return $this->simpleGetQuery('checksession', []);
    }

    /**
     * API seems bugged - returned true but password not changed
     *
     * @param string $newPassword
     * @return bool
     */
    public function resetApiPassword(string $newPassword): bool
    {
        $query = ['password' => $newPassword];
        return $this->simplePostQuery('pwdreset/api', $query);
    }
}
