<?php
/*
 * Copyright (c) 2024. Amedeo Lanza - ConsuLanza Informatica.
 * This software is licensed under GNU AGPLv3 - https://www.gnu.org/licenses/agpl-3.0.en.html.
 * For commercial use you can obtain a limited commercial license from the author.
 *
 *
 */

declare(strict_types=1);

namespace Inforisorse\SkebbyApi\Driver\Api\Traits;

use GuzzleHttp\Exception\GuzzleException;
use Inforisorse\SkebbyApi\Exceptions\ApiLoginException;
use Inforisorse\SkebbyApi\Exceptions\ValidationException;
use Inforisorse\SkebbyApi\Helper\Validator;
use stdClass;

/**
 * methods related to https://developers.skebby.it/?php#sms-history-apiget
 */
trait SmsHistoryApi
{
    /**
     * Returns the user’s SMS messages history
     *
     * @param string $fromDate
     * @param string $toDate
     * @param string $timezone
     * @param int $pageNumber
     * @param int $pageSize
     * @return array
     * @throws ApiLoginException
     * @throws GuzzleException
     */
    public function getSmsHistory(string $fromDate, string $toDate = '', string $timezone = '', int $pageNumber = 0, int $pageSize = 0): stdClass
    {
        $query = ['from' => $fromDate];
        $query = array_merge($query, $this->historyQueryOptionalParams($toDate, $timezone, $pageNumber, $pageSize));

        return $this->simpleGetQuery('smshistory', $query);
    }

    /**
     * @param string $toDate
     * @param string $timezone
     * @param int $pageNumber
     * @param int $pageSize
     * @return array
     */
    private function historyQueryOptionalParams(string $toDate = '', string $timezone = '', int $pageNumber = 0, int $pageSize = 0): array
    {
        $query = [];

        if (!empty($toDate)) {
            $query['to'] = $toDate;
        }
        if (!empty($timezone)) {
            $query['timezone'] = $timezone;
        }
        if (!empty($pageNumber)) {
            $query['pageNumber'] = $pageNumber;
        }
        if (!empty($pageSize)) {
            $query['pageSize'] = $pageSize;
        }

        return $query;
    }

    /**
     * Returns the user’s SMS messages history for the specified recipient
     *
     * @param string $recipient
     * @param string $fromDate
     * @param string $toDate
     * @param string $timezone
     * @param int $pageNumber
     * @param int $pageSize
     * @return stdClass
     * @throws \Inforisorse\SkebbyApi\Exceptions\ValidationException
     */
    public function getRecipientHistory(string $recipient, string $fromDate, string $toDate = '', string $timezone = '', int $pageNumber = 0, int $pageSize = 0): stdClass
    {
        $query = [
            'recipient' => Validator::phoneNumber($recipient, 'recipient'),
            'from' => $fromDate
        ];
        $query = array_merge($query, $this->historyQueryOptionalParams($toDate, $timezone, $pageNumber, $pageSize));

        return $this->simpleGetQuery('rcptHistory', $query);
    }

    /**
     * After sending an sms campaign via API containing a Rich sms shortened link, this api returns that sms link’s
     * opening statistics. Keep in mind that you need to use a unique order_id when sending and store it to request in
     * the near future.
     *
     * @param string $orderId
     * @return stdClass
     * @throws ValidationException
     */
    public function getRichSmsHistory(string $orderId): stdClass

    {
        $query = ['order_id' => Validator::orderId($orderId, 'order_id')];
        return $this->simpleGetQuery('smsrich/statistics', $query);
    }
}
