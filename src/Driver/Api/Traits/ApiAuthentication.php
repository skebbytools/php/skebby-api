<?php
/*
 * Copyright (c) 2024. Amedeo Lanza - ConsuLanza Informatica.
 * This software is licensed under GNU AGPLv3 - https://www.gnu.org/licenses/agpl-3.0.en.html.
 * For commercial use you can obtain a limited commercial license from the author.
 *
 *
 */

namespace Inforisorse\SkebbyApi\Driver\Api\Traits;

use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use Inforisorse\SkebbyApi\Driver\Api\Skebby;
use Inforisorse\SkebbyApi\Enums\AuthenticationType;
use Inforisorse\SkebbyApi\Exceptions\ApiLoginException;
use Inforisorse\SkebbyApi\Exceptions\EmptyCredentialException;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

trait ApiAuthentication
{
    protected AuthenticationType $authenticationType = AuthenticationType::Token;
    protected array $apiCredentials = [];

    public function getAuthenticationType(): AuthenticationType
    {
        return $this->authenticationType;
    }

    public function setAuthenticationType(AuthenticationType $authenticationType): Skebby
    {
        $this->authenticationType = $authenticationType;
        return $this;
    }

    /**
     * @throws EmptyCredentialException
     */
    public function setBasicAuthCredentials(string $username, string $password): Skebby
    {
        $this->apiCredentials = [];
        $username = trim($username);
        if (empty($username) || empty($password)) {
            throw new EmptyCredentialException('Basic auth credentials cannot be empty.');
        }
        $this->username = $username;
        $this->password = $password;
        return $this;
    }

    /**
     * @throws GuzzleException
     * @throws ApiLoginException
     */
    protected function authenticate(): self
    {
        if ($this->isAuthenticated()) {
            return $this;
        }
        try {
            $result = $this->client
                ->request(
                    'GET',
                    $this->authenticationType->url(),
                    [
                        'auth' => [$this->username, $this->password],
                        'headers' => [
                            'Accept' => 'application/json',
                        ],
                    ]
                );
        } catch (ClientException $e) {
            $responseMessage = match ($e->getResponse()->getStatusCode()) {

                // should never match since username and password are checked in setBasicAuthCredentials()
                ResponseAlias::HTTP_BAD_REQUEST => $e->getMessage(),

                // Basic Auth returns 404 if user not found or wrong password
                ResponseAlias::HTTP_NOT_FOUND => 'Check basic auth credentials. Either username was not found or password mismatch.'
            };
            throw new ApiLoginException(sprintf('API Login client exception. %s', $responseMessage));
        }
        if ($result->getStatusCode() !== ResponseAlias::HTTP_OK) {
            // unexpected return status
            throw new ApiLoginException(sprintf('API Login failed with status code: %s.', $result->getStatusCode()));
        }

        $body = $result->getBody()->getContents();
        $this->apiCredentials = explode(';', $body);

        return $this;
    }

    protected function isAuthenticated(): bool
    {
        return !empty($this->apiCredentials[0]) && !empty($this->apiCredentials[1]);
    }

}
