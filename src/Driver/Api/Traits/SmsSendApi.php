<?php
/*
 * Copyright (c) 2024. Amedeo Lanza - ConsuLanza Informatica.
 * This software is licensed under GNU AGPLv3 - https://www.gnu.org/licenses/agpl-3.0.en.html.
 * For commercial use you can obtain a limited commercial license from the author.
 *
 *
 */

declare(strict_types=1);

namespace Inforisorse\SkebbyApi\Driver\Api\Traits;

use GuzzleHttp\Exception\GuzzleException;
use Inforisorse\SkebbyApi\Driver\Message;
use Inforisorse\SkebbyApi\Exceptions\ApiLoginException;
use Inforisorse\SkebbyApi\Exceptions\ValidationException;
use Inforisorse\SkebbyApi\Helper\Validator;
use stdClass;

/**
 * methods related to https://developers.skebby.it/?php#sms-send-api
 */
trait SmsSendApi
{

    /**
     * Possible Skebby result status codes:
     *  201    SMSs have been scheduled for delivery.
     *  400    Invalid input. Details are in the response body
     *  401    [Unauthorized] User_key, Token or Session_key are invalid or not provided
     *  404    [Not found] The User_key was not found
     *
     *  note that executes authenticate() if not already authenticated. authhenticate() will trow ApiLoginException
     *  instead returning an 4xx status code. The caller should then trap such exception.
     *
     * @param Message $message
     * @return int
     * @throws ApiLoginException
     * @throws GuzzleException
     */
    public function sendSms(Message $message): stdClass
    {
        return $this->simplePostQuery('sms', $message->getPayload());
    }

    /**
     * Get informations on the SMS delivery status of the given order_id.
     *
     * @param string $orderId
     * @return array
     * @throws GuzzleException
     */
    public function getSmsMessageState(string $orderId): stdClass
    {
        $query = ['order_id' => Validator::orderId($orderId, 'order_id')];
        return $this->simpleGetQuery('sms', $query);
    }

    /**
     * Deletes the SMS delivery process of the given order_id.
     *
     * @param string $orderId
     * @return array
     * @throws ValidationException
     */
    public function deleteScheduledMessage(string $orderId): stdClass
    {
        return $this->simpleDeleteQuery('sms', Validator::orderId($orderId, 'order_id'));
    }

    /**
     * Bulk Delete of all SMS delivery process of the user
     *
     * @return stdClass
     */
    public function bulkDeleteScheduledMessages(): stdClass
    {
        return $this->simpleDeleteQuery('sms/bulk');
    }

}
