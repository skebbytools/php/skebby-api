<?php
/*
 * Copyright (c) 2024. Amedeo Lanza - ConsuLanza Informatica.
 * This software is licensed under GNU AGPLv3 - https://www.gnu.org/licenses/agpl-3.0.en.html.
 * For commercial use you can obtain a limited commercial license from the author.
 *
 *
 */

declare(strict_types=1);

namespace Inforisorse\SkebbyApi\Driver\Api\Traits;

use GuzzleHttp\Exception\InvalidArgumentException;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

trait ApiResponseCheck
{
    protected function checkApiResponseStatus(int $statusCode, string $caller): void
    {
        if ($this->isSuccess($statusCode)) {
            return;
        }

        match ($statusCode) {
            ResponseAlias::HTTP_BAD_REQUEST => call_user_func(function () {
                $errorMessage = '';
                throw new InvalidArgumentException($errorMessage);
            }),

            ResponseAlias::HTTP_UNAUTHORIZED => call_user_func(function () {
                $errorMessage = 'User_key, Token or Session_key are invalid or not provided.';
                throw new InvalidArgumentException($errorMessage);
            }),

            ResponseAlias::HTTP_NOT_FOUND => call_user_func(function () {
                $errorMessage = 'The User_key was not found.';
                throw new InvalidArgumentException($errorMessage);
            }),

            default => call_user_func(function ($statusCode, $caller) {
                $errorMessage = sprintf('Unexpected status code %s from %s() API call', $statusCode, $caller);
                throw new InvalidArgumentException($errorMessage);
            }),
        };
    }

    protected function isSuccess(int $status): bool
    {
        return $status > 199 && $status < 299;
    }
}
