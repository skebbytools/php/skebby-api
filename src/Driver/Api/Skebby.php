<?php
/*
 * Copyright (c) 2024. Amedeo Lanza - ConsuLanza Informatica.
 * This software is licensed under GNU AGPLv3 - https://www.gnu.org/licenses/agpl-3.0.en.html.
 * For commercial use you can obtain a limited commercial license from the author.
 *
 */

declare(strict_types=1);

namespace Inforisorse\SkebbyApi\Driver\Api;

use GuzzleHttp\Client;
use Inforisorse\SkebbyApi\Driver\Api\Traits\ApiAuthentication;
use Inforisorse\SkebbyApi\Driver\Api\Traits\ApiResponseCheck;
use Inforisorse\SkebbyApi\Driver\Api\Traits\ContactsGroupsApi;
use Inforisorse\SkebbyApi\Driver\Api\Traits\SmsHistoryApi;
use Inforisorse\SkebbyApi\Driver\Api\Traits\SmsSendApi;
use Inforisorse\SkebbyApi\Driver\Api\Traits\UserApi;
use Inforisorse\SkebbyApi\Enums\AuthenticationType;
use Inforisorse\SkebbyApi\Exceptions\EmptyCredentialException;
use stdClass;

class Skebby
{
    use ApiAuthentication, ApiResponseCheck, SmsSendApi, SmsHistoryApi, UserApi, ContactsGroupsApi;

    protected string $base_uri = 'https://api.skebby.it/API/v1.0/REST/';
    private Client $client;

    /**
     * @throws EmptyCredentialException
     */
    public function __construct(
        private string     $username,
        private string     $password,
        AuthenticationType $authenticationType = AuthenticationType::Token
    )
    {
        $this->setBasicAuthCredentials($this->username, $this->password);
        $this->authenticationType = $authenticationType;
        $this->client = new Client(['base_uri' => $this->base_uri]);
    }

    protected function simpleGetQuery(string $path, array $query): mixed
    {
        $this->authenticate();

        $body = ['headers' => $this->prepareHeaders(true)];
        if (!empty($query)) {
            $body['query'] = $query;
        }

        $result = $this->client
            ->request(
                'GET',
                $path,
                $body,
            );
        $content = $result->getBody()->getContents();
        $this->checkApiResponseStatus($result->getStatusCode(), __METHOD__);
        return json_decode($content);
    }

    protected function prepareHeaders(bool $withContent = false): array
    {
        $result = [
            'Accept' => 'application/json',
            'User_key' => $this->apiCredentials[0],
            $this->authenticationType->authKeyName() => $this->apiCredentials[1],
        ];

        if ($withContent) {
            $result['Content-type'] = 'application/json';
        }

        return $result;
    }

    protected function simplePostQuery(string $path, array $body): mixed
    {
        $this->authenticate();

        $result = $this->client
            ->request(
                'POST',
                $path,
                [
                    'headers' => $this->prepareHeaders(true),
                    'json' => $body,
                ]
            );
        $this->checkApiResponseStatus($result->getStatusCode(), __METHOD__);

        return json_decode($result->getBody()->getContents());
    }

    protected function simpleDeleteQuery(string $path, string $id = ''): stdClass
    {
        $this->authenticate();

        if (!empty($id)) {
            $path = $path . '/' . $id;
        }

        $result = $this->client
            ->request(
                'DELETE',
                $path,
                [
                    'headers' => $this->prepareHeaders(),
                ]
            );
        $this->checkApiResponseStatus($result->getStatusCode(), __METHOD__);

        return json_decode($result->getBody()->getContents());
    }
}
