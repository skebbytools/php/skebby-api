<?php
/*
 * Copyright (c) 2024. Amedeo Lanza - ConsuLanza Informatica.
 * This software is licensed under GNU AGPLv3 - https://www.gnu.org/licenses/agpl-3.0.en.html.
 * For commercial use you can obtain a limited commercial license from the author.
 *
 *
 */

declare(strict_types=1);

namespace Inforisorse\SkebbyApi\Driver;

use Inforisorse\SkebbyApi\Driver\Contracts\ApiPayloadAbstract;
use Inforisorse\SkebbyApi\Exceptions\ValidationException;
use Inforisorse\SkebbyApi\Helper\Validator;

class Contact extends ApiPayloadAbstract
{

    /**
     * @param string $email
     * @param string $phoneNumber
     * @param string $name
     * @param string $surname
     * @param string $gender
     * @param string $fax
     * @param string $address
     * @param string $city
     * @param string $province
     * @param string $birthdate
     * @param string $promotiondate
     * @param string $rememberdate
     * @param string $zip
     * @param array $groupIds
     * @param string $custom1
     * @param string $custom2
     * @param string $custom3
     * @param string $custom4
     * @param string $custom5
     * @param string $custom6
     * @param string $custom7
     * @param string $custom8
     * @param string $custom9
     * @param string $custom10
     * @throws ValidationException
     */
    public function __construct(
        //The email of the contact
        protected string $email,

        // The phone number of the contact. The phone number numbers must be in the international format (+393471234567 or 00393471234567)
        protected string $phoneNumber,

        // The first name of the contact
        protected string $name = '',

        // The last name of the contact
        protected string $surname = '',

        // The gender of the contact
        protected string $gender = '',

        // The Fax number of the contact
        protected string $fax = '',

        // The address of the contact
        protected string $address = '',

        // The city of the contact
        protected string $city = '',

        // The province of the contact
        protected string $province = '',

        // The birth date of the contact
        // String [ddMMyy, yyyyMMdd, ddMMyyHHmm, yyyyMMddHHmmss, yyyy-MM-dd HH:mm, yyyy-MM-dd HH:mm.0
        protected string $birthdate = '',

        // An additional date field
        // String [ddMMyy, yyyyMMdd, ddMMyyHHmm, yyyyMMddHHmmss, yyyy-MM-dd HH:mm, yyyy-MM-dd HH:mm.0
        protected string $promotiondate = '',

        // An additional date field
        // String [ddMMyy, yyyyMMdd, ddMMyyHHmm, yyyyMMddHHmmss, yyyy-MM-dd HH:mm, yyyy-MM-dd HH:mm.0
        protected string $rememberdate = '',

        // The ZIP code of the contact
        protected string $zip = '',

        // The groups (Ids) in which the contact will be added
        protected array  $groupIds = [],

        // Custom field
        protected string $custom1 = '',

        // Custom field
        protected string $custom2 = '',

        // Custom field
        protected string $custom3 = '',

        // Custom field
        protected string $custom4 = '',

        // Custom field
        protected string $custom5 = '',

        // Custom field
        protected string $custom6 = '',

        // Custom field
        protected string $custom7 = '',

        // Custom field
        protected string $custom8 = '',

        // Custom field
        protected string $custom9 = '',

        // Custom field
        protected string $custom10 = '',
    )
    {
        $this->withEmailAndPhoneNumber($email, $phoneNumber);
    }

    /**
     * @throws ValidationException
     */
    public function withEmailAndPhoneNumber(string $email, string $phoneNumber): Contact
    {
        $email = trim($email);
        $phoneNumber = trim($phoneNumber);
        if (empty($email) && empty($phoneNumber)) {
            $message = 'At least one of the "email" and the "phoneNumber" fields has to be provided';
            throw new ValidationException($message);
        }

        if (!empty($mail)) {
            $this->email = Validator::email($email, 'email');
        }

        if (!empty($phoneNumber)) {
            $this->phoneNumber = Validator::phoneNumber($this->phoneNumber, 'phoneNumber');
        }
        return $this;
    }

    /**
     * @throws ValidationException
     */
    public function withName(string $name): Contact
    {
        $this->name = Validator::contactName($name, 'name');
        return $this;
    }

    /**
     * @throws ValidationException
     */
    public function withSurname(string $surname): Contact
    {
        $this->surname = Validator::contactName($surname, 'surname');
        return $this;
    }

    public function withGender(string $gender): Contact
    {
        $this->gender = $gender;
        return $this;
    }

    /**
     * @throws ValidationException
     */
    public function withFax(string $fax): Contact
    {
        $this->fax = Validator::phoneNumber($fax, 'fax');
        return $this;
    }

    /**
     * @throws ValidationException
     */
    public function withAddress(string $address): Contact
    {
        $this->address = Validator::address($address, 'address');
        return $this;
    }

    /**
     * @throws ValidationException
     */
    public function withCity(string $city): Contact
    {
        $this->city = Validator::city($city, 'city');
        return $this;
    }

    public function withProvince(string $province): Contact
    {
        $this->province = Validator::province($province, 'province');
        return $this;
    }

    /**
     * @throws ValidationException
     */
    public function withBirthdate(string $birthdate): Contact
    {
        $this->birthdate = Validator::packedDateTime($birthdate, 'birthdate');
        return $this;
    }

    /**
     * @throws ValidationException
     */
    public function withPromotiondate(string $promotiondate): Contact
    {
        $this->promotiondate = Validator::packedDateTime($promotiondate, 'promotiondate');
        return $this;
    }

    /**
     * @throws ValidationException
     */
    public function withRememberdate(string $rememberdate): Contact
    {
        $this->rememberdate = Validator::packedDateTime($rememberdate, 'remeberdate');
        return $this;
    }

    /**
     * @throws ValidationException
     */
    public function withZip(string $zip): Contact
    {
        $this->zip = Validator::zipCode($zip, 'zip');
        return $this;
    }

    public function withGroupIds(array $groupIds): Contact
    {
        $this->groupIds = $groupIds;
        return $this;
    }

    public function withCustom1(string $custom1): Contact
    {
        $this->custom1 = $custom1;
        return $this;
    }

    public function withCustom2(string $custom2): Contact
    {
        $this->custom2 = $custom2;
        return $this;
    }

    public function withCustom3(string $custom3): Contact
    {
        $this->custom3 = $custom3;
        return $this;
    }

    public function withCustom4(string $custom4): Contact
    {
        $this->custom4 = $custom4;
        return $this;
    }

    public function withCustom5(string $custom5): Contact
    {
        $this->custom5 = $custom5;
        return $this;
    }

    public function withCustom6(string $custom6): Contact
    {
        $this->custom6 = $custom6;
        return $this;
    }

    public function withCustom7(string $custom7): Contact
    {
        $this->custom7 = $custom7;
        return $this;
    }

    public function withCustom8(string $custom8): Contact
    {
        $this->custom8 = $custom8;
        return $this;
    }

    public function withCustom9(string $custom9): Contact
    {
        $this->custom9 = $custom9;
        return $this;
    }

    public function withCustom10(string $custom10): Contact
    {
        $this->custom10 = $custom10;
        return $this;
    }

    protected function preparePayload(): void
    {
        $this->addPayloadParam('email', $this->email);
        $this->addPayloadParam('phoneNumber', $this->phoneNumber);

        $this->addStringParamIfNotEmpty('name');
        $this->addStringParamIfNotEmpty('surname');
        $this->addStringParamIfNotEmpty('gender');
        $this->addStringParamIfNotEmpty('fax');
        $this->addStringParamIfNotEmpty('address');
        $this->addStringParamIfNotEmpty('city');
        $this->addStringParamIfNotEmpty('province');
        $this->addStringParamIfNotEmpty('birthdate');
        $this->addStringParamIfNotEmpty('promotiondate');
        $this->addStringParamIfNotEmpty('rememberdate');
        $this->addStringParamIfNotEmpty('zip');

        if (!empty($this->groupIds)) {
            $this->addPayloadParam('groupIds', $this->groupIds);
        }

        $this->addStringParamIfNotEmpty('custom1');
        $this->addStringParamIfNotEmpty('custom2');
        $this->addStringParamIfNotEmpty('custom3');
        $this->addStringParamIfNotEmpty('custom4');
        $this->addStringParamIfNotEmpty('custom5');
        $this->addStringParamIfNotEmpty('custom6');
        $this->addStringParamIfNotEmpty('custom7');
        $this->addStringParamIfNotEmpty('custom8');
        $this->addStringParamIfNotEmpty('custom9');
        $this->addStringParamIfNotEmpty('custom10');
    }
}
