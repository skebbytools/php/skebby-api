<?php
/*
 * Copyright (c) 2024. Amedeo Lanza - ConsuLanza Informatica.
 * This software is licensed under GNU AGPLv3 - https://www.gnu.org/licenses/agpl-3.0.en.html.
 * For commercial use you can obtain a limited commercial license from the author.
 *
 */
declare(strict_types=1);

namespace Inforisorse\SkebbyApi\Helper;

use DateTime;
use Inforisorse\SkebbyApi\Enums\Encoding;
use Inforisorse\SkebbyApi\Enums\MessageType;
use Inforisorse\SkebbyApi\Exceptions\ValidationException;

class Validator
{
    /**
     * @throws ValidationException
     */
    public static function address(string $value, string $fieldname): string
    {
        return self::validateStringLength($value, $fieldname, 256);
    }

    /**
     * @throws ValidationException
     */
    protected static function validateStringLength(string $value, string $fieldname, int $maxLength, int $minLength = 0): string
    {
        $value = trim($value);
        if (strlen($value) > $maxLength) {
            $message = sprintf('Value "%s" for field "%s" exceeds lenght of %u chars', $value, $fieldname, $maxLength);
            throw new ValidationException($message);
        }
        if (strlen($value) < $minLength) {
            $message = sprintf('Value "%s" for field "%s" must be at least %u chars', $value, $fieldname, $minLength);
            throw new ValidationException($message);
        }
        return $value;
    }

    /**
     * @throws ValidationException
     */
    public static function campaignName(string $value, string $fieldname): string
    {
        return self::validateStringLength($value, $fieldname, 64);
    }

    /**
     * @throws ValidationException
     */
    public static function city(string $value, string $fieldname): string
    {
        return self::validateStringLength($value, $fieldname, 32);
    }

    /**
     * @throws ValidationException
     */
    public static function contactName(string $value, string $fieldname): string
    {
        return self::validateStringLength($value, $fieldname, 256);
    }

    public static function ianaTimeZone(string $value, string $fieldname): string
    {
        return $value;
    }

    /**
     * @throws ValidationException
     */
    public static function messageText(string $source, Encoding $encoding): string
    {
        $result = $source;
        if (strlen($result) > $encoding->maxTextLength()) {
            $message = sprintf("Your message text is %s chars long but message encoding '%s' allows a maximum of %s chars.", strlen($result), $encoding->value, $encoding->maxTextLength());
            throw new ValidationException($message);
        }
        return $result;
    }

    /**
     * @throws ValidationException
     */
    public static function orderId(string $value, $fieldname): string
    {
        $value = str_replace([' '], '', $value);
        $value = self::validateStringLength($value, $fieldname, 32);

        if (preg_match('/^[a-zA-Z0-9_.\-]+$/', $value) !== 1) {
            $message = sprintf('Invalid Order ID format: "%s" for field "%s". Allowed chars are letters, numbers, underscore, dot and dash', $value, $fieldname);
            throw new ValidationException($message);
        }
        return $value;
    }

    /**
     * @throws ValidationException
     */
    public static function packedDateTime(string $value, string $fieldname): string
    {
        $validDateFormats = [
            'dmy',
            'Ymd',
            'dmyHi',
            'YmdHis',
            'Y-m-d H:i',
            'Y-m-d H:i.0'
        ];

        foreach ($validDateFormats as $format) {
            if (self::validateDate($value, $format)) {
                return $value;
            }
        }
        $message = sprintf('Invalid date "%s" for field "%s". Allowed datetime formats are: %s', $value, $fieldname, implode("','", $validDateFormats));
        throw new ValidationException($message);
    }

    protected static function validateDate($date, $format): bool
    {
        $dateCheck = DateTime::createFromFormat($format, $date);
        return $dateCheck && $dateCheck->format($format) === $date;
    }

    /**
     * Format and validate phone number. Skebby requires number in the format:
     * +<country id><number>
     *
     * @param string $phoneNumber
     * @param string $fieldname
     * @return string
     * @throws ValidationException
     */
    public static function phoneNumber(string $phoneNumber, string $fieldname): string
    {
        $result = str_replace([' ', '(', ')'], '', $phoneNumber);
        if (strlen($result) === 0) {
            $message = sprintf('Value "%s" for field "%s" is not a valid phone number (empty).', $phoneNumber, $fieldname);
            throw new ValidationException($message);
        }
        if (str_starts_with($result, '00')) {
            $result = '+' . substr($result, 2);
        }
        if ($result[0] !== '+') {
            $message = sprintf('Value "%s" is not a valid phone number for field "%s" (missing country id).', $phoneNumber, $fieldname);
            throw new ValidationException($message);
        }
        return $result;
    }

    /**
     * @throws ValidationException
     */
    public static function province(string $value, string $fieldname): string
    {
        return self::validateStringLength($value, $fieldname, 32);
    }

    /**
     * @throws ValidationException
     */
    public static function sender(string $source, MessageType $messageType): string
    {
        $result = $source;
        if (!empty($source) && !$messageType->canForceSender()) {
            $message = sprintf('Message type "%s" does not allow setting of sender', $messageType->value);
            throw new ValidationException($message);
        }
        return $result;
    }

    /**
     * @throws ValidationException
     */
    public static function email(string $email, string $fieldname): string
    {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $message = sprintf('"%s" is not a valid email for field "%s"', $email, $fieldname);
            throw new ValidationException($message);
        }
        return $email;
    }

    /**
     * @throws ValidationException
     */
    public static function zipCode(string $value, string $fieldname): string
    {
        return self::validateStringLength($value, $fieldname, 16);
    }
}
