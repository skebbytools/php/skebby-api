<?php
/*
 * Copyright (c) 2024. Amedeo Lanza - ConsuLanza Informatica.
 * This software is licensed under GNU AGPLv3 - https://www.gnu.org/licenses/agpl-3.0.en.html.
 * For commercial use you can obtain a limited commercial license from the author.
 *
 */
declare(strict_types=1);

namespace Inforisorse\SkebbyApi\Enums;

enum Encoding: string
{
    case Gsm = 'gsm';
    case Ucs2 = 'ucs2';

    public function maxTextLength(): int
    {
        return match ($this) {
            Encoding::Gsm => 1000,
            Encoding::Ucs2 => 450
        };
    }
}
