<?php
/*
 * Copyright (c) 2024. Amedeo Lanza - ConsuLanza Informatica.
 * This software is licensed under GNU AGPLv3 - https://www.gnu.org/licenses/agpl-3.0.en.html.
 * For commercial use you can obtain a limited commercial license from the author.
 *
 */
declare(strict_types=1);

namespace Inforisorse\SkebbyApi\Enums;

enum AuthenticationType: string
{
    case Login = 'login';
    case Token = 'token';

    public function description(): string
    {
        return match ($this) {
            AuthenticationType::Login => 'Use User Token',
            AuthenticationType::Token => 'Use API Token',
        };
    }

    public function url(): string
    {
        return match ($this) {
            AuthenticationType::Login => 'login',
            AuthenticationType::Token => 'token',
        };
    }

    public function authKeyName(): string
    {
        return match ($this) {
            AuthenticationType::Login => 'Session_key',
            AuthenticationType::Token => 'Access_token',
        };
    }
}
