<?php
declare(strict_types=1);
/*
 * Copyright (c) 2024. Amedeo Lanza - ConsuLanza Informatica.
 * This software is licensed under GNU AGPLv3 - https://www.gnu.org/licenses/agpl-3.0.en.html.
 * For commercial use you can obtain a limited commercial license from the author.
 *
 */

namespace Inforisorse\SkebbyApi\Enums;

enum MessageType: string
{
    case Basic = 'SI';
    case Classic = 'TI';
    case ClassicPlus = 'GP';

    public function canForceSender(): bool
    {
        return match ($this) {
            MessageType::Basic => false,
            MessageType::Classic, MessageType::ClassicPlus => true,
        };
    }

}
