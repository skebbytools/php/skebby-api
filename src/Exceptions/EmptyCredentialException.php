<?php
/*
 * Copyright (c) 2024. Amedeo Lanza - ConsuLanza Informatica.
 * This software is licensed under AGNU GPLv3 - https://www.gnu.org/licenses/agpl-3.0.en.html.
 * For commercial use you can obtain a limited commercial license from the author.
 *
 */
declare(strict_types=1);

namespace Inforisorse\SkebbyApi\Exceptions;

use Exception;

class EmptyCredentialException extends Exception
{

}
