# Skebby API issues

## HTTP headers

Negli esempi viene impostato l'header 'content-type' anche per le query effettuate via GET

## GET /API/v1.0/REST/dashboard

Utilizzando un utente API restituisce sempre null, sia con autenticazione login che token


## POST /API/v1.0/REST/group

crea il gruppo ma restituisce null invece di 'Location': '/group/{group_id}'


## GET /API/v1.0/REST/groups?pageNumber=PAGE&pageSize=SIZE

richiede header content-type pur essendo una richiesta GET

