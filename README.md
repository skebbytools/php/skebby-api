# Inforisorse Skebby API for PHP

_**[Skebby](https://www.skebby.it/)**_ is a SMS service provider that offers sending and managing messages.
The _**[Inforisorse Skebby API for PHP](https://gitlab.com/skebbytools/php/skebby-api)**_ package aims to create a
library for simple interfacing Skebby's API from
PHP. _**Inforisorse Skebby API**_ provides a set of
classes and methods to manage your Skebby's account and services via API without having to directly mess with HTTP
protocol, request formatting and such typical, boring, aspects of service's API consuming.

_**[Skebby's API reference](https://developers.skebby.it)**_ may be found at: https://developers.skebby.it

## Laravel companion package

For **[Laravel](https://laravel.com/)** projects is also available the
_**[Inforisorse Skebby for Laravel](https://gitlab.com/skebbytools/php/skebby-laravel)**_ package which provides
integration for Laravel framework with the specific _SkebbyChannel_ channel to be used with notifications.

## Licensing

Inforisorse Skebby API for PHP is Copyright of **_Amedeo Lanza - ConsuLanza Informatica_** and is released under
_**[GNU Affero General Public License Version 3](https://www.gnu.org/licenses/agpl-3.0.en.html)**_.

If you are interested in developing non-agpl derivates or use this software in non-agpl server software, You may obtain
commercial licenses by contacting the author at info@consulanza.it.

## Required PHP version

Inforisorse Skebby API requires PHP version 8.2 or later

## Required PHP packages

- symfony/http-foundation
- guzzlehttp/guzzle

## Install

In order to install the package you nedd to add the repositoriy in composer.json:

```json
"repositories": [
    {
        "type": "vcs",
        "url":  "https://gitlab.com/skebbytools/php/skebby-api"
    }
]


## Simplified authentication

The driver setup requires only the account's username and password; each method that perform an API call will check
if already authenticated and will call authenticate() if needed. The bearer authentication used for API calls is handled
internally and will reflect the $messageType value (default to Token). You don't need to know the API authentication
mechanism and flow since it is handled by the driver.

## Simplified API service call

The driver provides methods reflecting available API calls, just giving a PHP-ish interface and hiding any interaction
with the internal HTTP client. Just some adjustements in the naming in order to have more descriptive method names.

## The _Message_ class

Inforisorse\SkebbyApi\Driver\Message is a class dedicated to SMS message build; message properties may be set with
contructor or via specific methods. You may think at the Message class as a sort of DTO

To send a message you will need to instantiate the message and then pass it to the driver's sendSms() method. The driver
is unaware of the API call payload requirements and performs the api call passing the message payload() array to the
internal HTTP client (GuzzleHttp\Client)

### Message constructor

```php
public function __construct(
        // The body of the message. *Message max length could be 160 chars when using low-quality SMSs.
        // New line char needs to be codified as “\n”.
        protected string      $text,

        // A list of recipents phone numbers. The recipients’ numbers must be in the international format
        // (+393471234567 or 00393471234567)
        protected array       $recipients,

        // The Sender name. If the message type allows a custom TPOA and the field is left empty, the user’s
        // preferred TPOA is used. Must be empty if the message type does not allow a custom TPOA
        protected string      $sender = '',

        // The type of SMS.
        protected MessageType $messsageType = MessageType::Medium,

        // The messages will be sent at the given scheduled time
        // String [ddMMyy, yyyyMMdd, ddMMyyHHmm, yyyyMMddHHmmss, yyyy-MM-dd HH:mm, yyyy-MM-dd HH:mm.0]
        protected string      $scheduled_delivery_time = '',

        // Optional timezone applied to scheduled_delivery_time date
        protected string      $scheduled_delivery_timezone = '',

        // Specifies a custom order ID
        // String (max 32 chars, accepts only any letters, numbers, underscore, dot and dash)
        protected string      $order_id = '',

        //Returns the number of used sms credits. Note that it can be greater than sent SMS number: i.e. when
        // message is more than 160 chars long, each SMS will consume more than one credit according to its length
        protected bool        $returnCredits = false,

        // Returns the number of remaining SMS credits for the quality used in the sent SMS and for the default
        //user nation
        protected bool        $returnRemaining = false,

        // Sending to an invalid recipient does not block the operation
        protected bool        $allowInvalidRecipients = false,

        // The SMS encoding. Use UCS2 for non standard character sets
        // String (“gsm” or “ucs2”)
        protected Encoding    $encoding = Encoding::Gsm,

        // The id of the published page. Also add the %PAGESLINK____________% placeholder in the message body
        protected ?int        $id_landing = null,

        // The campaign name (max 64 characters)
        protected string      $campaign_name = '',

        // The number of maximum fragments allowed per sms
        protected int         $max_fragments = self::DEFAULT_MAX_FRAGMENTS,

        // True, truncates any message exceeding max_fragments, False doesn’t send any overly long sms
        protected bool        $truncate = true,

        // Period after which the SMS message will be deleted from the SMS center. Please note the set expiry
        // time may not be honoured by some mobile networks.
        protected ?int        $validity_period_min = null,

        // The url where the rich url redirects. Also add the %RICHURL________% placeholder in the message body
        protected string      $richsms_url = ''
    )
```

only $text, and $recipient are required , all other values defaults to Skebby's API reference values if not set.

### Message methods

Set the recipients (targets) list to the specified array.

```php
public function withRecipients(array $recipients): Message
```

Add a single recipient to recipients' list

```php
public function addRecipient(string $recipient): Message
```

Set text of message.

```php
public function withText(string $text): Message
```

Set message sender. Require MessageType Medium or Low (Skebby requirement), the driver will
ignore sender if MessageType does not permit setting.

```php
public function withSender(string $sender): Message
```

Set the message quality property. Note that MessageType Low will prevent setting of sender.

```php
 public function withMesssageType(MessageType $messageType): Message
```

The messages will be sent at the given scheduled time. Valid formats:

| pattern            | PHP datetime format | example            |
|--------------------|---------------------|--------------------|
| ddMMyy             | dmy                 | 311223             |
| yyyyMMdd           | Ymd                 | 20231231           |
| ddMMyyHHmm         | dmyHi               | 3112232359         |
| yyyyMMddHHmmss     | YmdHis              | 20231231235959     |
| yyyy-MM-dd HH:mm   | Y-m-d H:i           | 2023-12-31 23:29   |
| yyyy-MM-dd HH:mm.0 | Y-m-d H:i.0         | 2023-12-31 23:29.0 |

```php
public function withScheduledDeliveryTime(string $scheduled_delivery_time): Message
```

Set optional timezone applied to scheduled_delivery_time date

```php
public function withScheduledDeliveryTimezone(string $scheduled_delivery_timezone): Message
```

Specifies a custom order ID. String (max 32 chars, accepts only any letters, numbers, underscore, dot and dash)

```php
public function withOrderId(string $order_id): Message
```

Returns the number of used sms credits. Note that it can be greater than sent SMS number: i.e. when message is more than
160 chars long, each SMS will consume more than one credit according to its length

```php
public function withReturnCredits(bool $returnCredits = true): Message
```

Returns the number of remaining SMS credits for the quality used in the sent SMS and for the default user nation

```php
public function withReturnRemaining(bool $returnRemaining = true): Message
```

Sending to an invalid recipient does not block the operation. Note that the methods of the Message class do not allow
to set invalid recipients.

```php
public function withAllowInvalidRecipients(bool $allowInvalidRecipients): Message
```

Set the SMS encoding. Use UCS2 for non-standard character sets

```php
public function withEncoding(Encoding $encoding): Message
```

The id of the published page. Also add the %PAGESLINK____________% placeholder in the message body

```php
public function withIdLanding(?int $id_landing): Message
```

Set the campaign name (max 64 characters)

```php
public function withCampaignName(string $campaign_name): Message
```

Set the number of maximum fragments allowed per sms

```php
public function withMaxFragments(int $max_fragments): Message
```

True, truncates any message exceeding max_fragments, False does not send any overly long sms

```php
public function withTruncate(bool $truncate): Message
```

Set the period after which the SMS message will be deleted from the SMS center. Please note the set expiry time may not
be
honoured by some mobile networks.

```php
public function withValidityPeriodMin(?int $validity_period_min): Message
```

Set the url where the rich url redirects. Also add the %RICHURL________% placeholder in the message body

```php
public function withRichsmsUrl(string $richsms_url): Message
```

Returns the paylod array used by Skebby driver to send the message.

```php
public function payload(): array
```

## The _Skebby_ driver class

The driver class uses an internal GuzzleHttp\Client instance to permorm API calls.

### The driver constructor

```php
 public function __construct(
        private string     $username,
        private string     $password,
        AuthenticationType $authenticationType = AuthenticationType::Token
    )
```

the constructor requires $username and $password, needed to obtain the API credentials for bearer token authentication.
The username and password will be used to perform the Skebby's API call to gather the API credentials.

Api credentials may be used for aithentication via _**session key**_ or _**user token**_, which require different
endpoint and parameters. The driver hides this boring operation by issuing the correct call based on the value
of $authenticationType.

You'll never need to perform any manual call to authenticate since the process is embedded in any API method and called
when needed.

### Driver methods

Driver public methods are grouped in traits reflecting Skebby's documentation API grouping.

#### Authentication

Authentication methods do not directly reflect Skebby API since skebby for php hides the internal flow and just requires
username and password.

Basic authentication credentials may be forced without re-instantiating the driver.

```php
public function setBasicAuthCredentials(string $username, string $password): Skebby
```

Returns the current bearer authentication type.

```php
public function getAuthenticationType(): AuthenticationType
```

Set the current bearer authentication type.

```php
public function setAuthenticationType(AuthenticationType $authenticationType): Skebby
```

#### SMS send API

Send a message. The message must be in instance of Inforisorse\SkebbyApi\Driver\Message.

```php
public function sendSms(Message $message): stdClass
```

Get informations on the SMS delivery status of the given order_id.

```php
public function getSmsMessageState(string $orderId): stdClass
```

Deletes the SMS delivery process of the given order_id.

```php
public function deleteScheduledMessage(string $orderId): stdClass
```

Bulk Delete of all SMS delivery process of the user

```php
public function bulkDeleteScheduledMessages(): stdClass
```

#### SMS History API

Gets the user’s SMS messages history.

```php
public function getSmsHistory(string $fromDate, string $toDate = '', string $timezone = '', int $pageNumber = 0, int $pageSize = 0): stdClass
```

Gets the user’s SMS messages history for the specified recipient.

```php
public function getRecipientHistory(string $recipient, string $fromDate, string $toDate = '', string $timezone = '', int $pageNumber = 0, int $pageSize = 0): stdClass
```

After sending an sms campaign via API containing a Rich sms shortened link, this api returns that sms link’s opening
statistics. Keep in mind that you need to use a unique order_id when sending and store it to request in the near future.

```php
public function getRichSmsHistory(string $orderId): stdClass
```

## Examples

```php
// the driver setup will do some formal check in order to avoid login fails (e.g. non-empty username and password)
$driver = new Skebby($skebbyUsername, $skebbyPassword);

// recipients must have country-id with either '+' or '00' prefix ('00' wil be converted to '+')
$message = new Message('some interesting text', ['00393471234567', '+3934610123456']);

// send the message
$driver->sendSms($message);
```

```php
$list = [
  'some text' => ['00393471234567', '+3934610123456'],
  'some other text' => ['+393477654321']
];
$driver = new Skebby($skebbyUsername, $skebbyPassword);

foreach ($list as $text=>$recipients) {
  $driver->sendSms(new Message($text, $recipients));
}

$history = $driver->getSmsHistory('310123');

$history = $driver->getSmsHistory('2023-01-31');

```
